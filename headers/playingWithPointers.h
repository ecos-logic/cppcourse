#ifndef CPPCOURSE_PLAYINGWITHPOINTERS_H
#define CPPCOURSE_PLAYINGWITHPOINTERS_H
#include "myflix.h"

void pointersToBasicTypesAndStructs();
void pointersToFunctions();


float circleAreaCalculator(float radius);
float circlePerimeterCalculator(float radius);
float circlePerimeterCalculator(float radius);
void pointersAndArrays();
void byValueFunction(movie movie);
void byReferenceFunction(movie *movie);

void pointersAndArraysAndStructs();

#endif //CPPCOURSE_PLAYINGWITHPOINTERS_H
