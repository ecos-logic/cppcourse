#include <string>

using namespace std;

#ifndef CPPCOURSE_MYFLIX_H
#define CPPCOURSE_MYFLIX_H

#define MOVIE_LENGTH 10
#define SERIES_LENGTH 5
#define CAST_LENGTH 5
#define CHAPTER_LENGTH 10

struct movie
{
    string title;
    int length;
    string director;
    string cast[CAST_LENGTH];
};

struct castList{
    string name="";
    castList *next = nullptr;
};

struct dynamicMovie
{
    string title;
    int length;
    string director;
    castList cast;
};

struct chapter
{
    string title;
    int length;
};

struct series
{
    string title;
    chapter chapters[CHAPTER_LENGTH];
};

void insertMovie(movie movies[MOVIE_LENGTH]);

void insertSeries(series seriesCollection[SERIES_LENGTH]);

void showMovies(movie movies[MOVIE_LENGTH]);

void showSeries(series seriesCollection[SERIES_LENGTH]);

void showIntercalate(movie movies[MOVIE_LENGTH], series seriesCollection[SERIES_LENGTH]);

void showMovie(movie theMovie, int i);

string movieAsString(const movie &movie);

string myReplace(string from,string what,string newString);

void readFromFile(movie *movies);

#endif //CPPCOURSE_MYFLIX_H
