//
// Created by pvergara on 31/12/22.
//
#include <string>

using namespace std;
#ifndef CPPCOURSE_THEME7_H
#define CPPCOURSE_THEME7_H
struct date {
    int day;
    int month;
    int year;
};

struct book {
    string title;
    char author [100];
    date publishing_date;
    float cost;
    int page_counter;
};

union temperature_kind {
    float cel;
    float fah;
    float kel;
};
#endif //CPPCOURSE_THEME7_H
