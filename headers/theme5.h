//
// Created by pvergara on 30/12/22.
//

#ifndef CPPCOURSE_THEME5_H
#define CPPCOURSE_THEME5_H
int absoluteValue(int x);
void interchange(int a, int b);
int factorial (int n);
int pow (int b, int n);
#endif //CPPCOURSE_THEME5_H
