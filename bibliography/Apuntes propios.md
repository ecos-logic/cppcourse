Apuntes propios
===============


Cuidado con los paréntesis (los carga el diablo :-D )

Recalcar la importancia de la precisión de las palabras. Ejemplos 
- No confundir algoritmo con proceso
- Cadenas (de caracteres)=>Strings
	
Camel case (Hola!=hola!=hOla)

OJO CON  las palabras completas versus palabras que, por el uso se acortan:
    Cadenas (de caracteres)
    Variables (de ámbito local... aunque existen muchos mas tipos de variables por el ámbito al que pertenecen)

**¿qué cojon...!!!!! XML LENGUAJE DE PROGRAMACIÓN?!?!?!?!?!??**

**Mentira... Java NO SE COMPILA** ESTRICTAMENTE!!!! (se pasa a código "Byte code" que es interpretado -a toda leche- por la JVM)

## Buenas prácticas en C (y en casi cualquier lenguaje)
- Usar cin y cout mejor que printf y scanf
- Indentar
- No olvides las cabeceras
- Todo lo que se abre se cierra al instante (paréntesis, comillas, llaves)
- Intentar abrir y cerrar llaves nada mas empezar con un if, while, do...while, for, método/función o clase
- Si el IDE te lo permite, limpia de warnings el fichero en el que te encuentras
- Control de versiones, Control de versiones y Control de versiones
- *Teclas rápidas (cuanto antes mejor)*
-  Inglés, inglés, inglés.
-  Clean
-  Pruebas!!!!!
-  Pomodoro

## Palabros
- Sintáctica
- Semántica
- Código fuente
- Lenguaje máquina
- Código máquina
- Lenguajes simbólicos
- ¿programa objeto?
- Compilación 
- Enlazado
- interpretación
- Transpilación
- Paradigmas de programación
- Diagramas de flujo
- Pseudocódigo
- Tablas de verdad (3:16)
- procedimientos vs funciones
- *recursividad*
-  Punto de inicio de un programa
-  Bibliotecas/librerías/libraries
-  Cobertura
-  Complejidad ciclomática
-  Caja blanca/Caja negra
-  Estructuras/registros/structs
-  Campo
-  Estructuras anidadas
-  Uniones
-  Paso por referencia
- Punteros
    - Direccionamiento
    - Indireccionamiento
- Entrada/Salida estándar

## Variables 
*Definición clásica (yo mismo la uso)*

> [...]
> 
> Cada dato debe vir definido por:
> - Nome: permite distinguilo dos restantes elementos.
> - Tamaño: indica a cantidade de caracteres ou números que se poden utilizar para definir o seu valor.
> - Tipo: describe se o elemento está constituído por caracteres alfabéticos, numéricos ou símbolos especiais.
>
> [...]

*Definición ampliada*

**El ámbito de una variable** también es muy importante puesto que determinan dos características muy importantes en la variables:
- El *ciclo de vida* (durante cuanto tiempo va a estar disponible en memoria) de esa variable 
- El *alcance* (o, de nuevo, el ámbito) que puede deter la variable. Como ejemplo, una variable con **ámbito local**  *dentro de un if*  sólo podrá ser escrita o leída dentro del cuerpo de dicho if (y si hay subramificaciones o bucle dentro del mismo, también ahí), Otro ejemplo sería una variable (de ámbito) "parámetro de un método/función" que, como una variable "de ámbito local" iniciada al principio de un método/función, serán accesibles dentro de todos los "subbloques" del método

##### Ejemplo 1 (variable -de ámbito- local dentro de un *bloque if*)

```c++
if(strcmp(user,"user1")==0)
{
    boolean hasUser = true;
    int counter = 0;
    while(counter<10)
    {
        counter++; //counter can be used on this block because is inside if block
        if(hasUser)
        {
            printf("hello!!!");
        }
    }
}
```

##### Ejemplo 2 (variable -de ámbito- local, parámetros de un método o función)

```c++
void doingThinks(int parameter) 
{
    printf("%d",parameter); //_parameter_ on the "main block"
    char user[]="user1"; //defining _user_ on the "main block"
    if(strcmp(user,"user1")==0) //using _user_ on the beginning of the "if block"
    {
        printf("%d",parameter); //_parameter_ on the "if block"
        bool hasUser = true;
        int counter = 0;
        while(counter<10)
        {
            printf("%d",parameter);  //_parameter_ on the "while block"
            counter++;
            if(hasUser)
            {  //2nd if block
                printf("%d %s",parameter, "hello!!! \n");
            }
        }
    }
}
```

## Tipos de datos
