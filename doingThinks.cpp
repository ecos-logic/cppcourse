#include <iostream>
#include <cstring>

void doingThinks(int parameter) {
    printf("%d",parameter); //_parameter_ on the "main block"
    char user[]="user1"; //defining _user_ on the "main block"
    if(strcmp(user,"user1")==0) //using _user_ on the beginning of the "if block"
    {
        printf("%d",parameter); //_parameter_ on the "if block"
        bool hasUser = true;
        int counter = 0;
        while(counter<10)
        {
            printf("%d",parameter);  //_parameter_ on the "while block"
            counter++;
            if(hasUser)
            {  //2nd if block
                printf("%d %s",parameter, "hello!!! \n");
            }
        }
    }
}