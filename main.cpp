#include <iostream>

#define ELF 1
#define DWARF 2
#define WARRIOR 3
#define MAGICIAN 4
#define HOBBIT 5

#define CREATURE_TYPES_LENGTH 5
#define GENDERS_LENGTH 2

using namespace std;

int main() {
    string menuOptions[] = {
            "[" + to_string(ELF) + "] Elf" + "\n",
            "[" + to_string(DWARF) + "] Dwarf" + "\n",
            "[" + to_string(WARRIOR) + "] Warrior/ess" + "\n",
            "[" + to_string(MAGICIAN) + "] Magician" + "\n",
            "[" + to_string(HOBBIT) + "] Hobbit" + "\n"
    };
    unsigned long menuOptionsLength = CREATURE_TYPES_LENGTH;

    cout << "Hello creature! Tell me your name: ";
    string name;
    getline(cin, name);
    cout << endl;

    cout << "Choose between 1 and " << to_string(menuOptionsLength) << endl;


    for(int i=1; i <= menuOptionsLength; i++) {
        cout << menuOptions[i-1];
    }

    int typeOfCreature;
    do{
        cin >> typeOfCreature;
    }while(typeOfCreature<1 || typeOfCreature > menuOptionsLength);
    cout << endl;

    cout << "Choose between 1 and 2: " << endl;

    cout << "[1] Female" << endl;
    cout << "[2] Male" << endl;

    int gender;
    do{
        cin >> gender;
    }while(gender!=1 && gender!=GENDERS_LENGTH);
    cout << endl;

    string salutation[GENDERS_LENGTH][CREATURE_TYPES_LENGTH] = {
            //Female
            "Greetings " + name + ", the great elf",
            "Greetings " + name + ", the great dwarf",
            "Greetings " + name + ", the great she-warrior",
            "Greetings " + name + ", the great magician",
            "Greetings " + name + ", the great hobbit",
            //Male
            "Greetings " + name + ", the great elf",
            "Greetings " + name + ", the great dwarf",
            "Greetings " + name + ", the great he-warrior",
            "Greetings " + name + ", the great magician",
            "Greetings " + name + ", the great hobbit",
    };

    cout << salutation[gender-1][typeOfCreature-1] << endl;

    return 0;
}