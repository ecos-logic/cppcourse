#include <iostream>
using namespace std;

int theme3_1(){
    int number;

    cin >> number;
    cout << "The double of the number is: " << number * 2 << endl;

    return 0;
}

int theme3_area(){
    int width;
    int height;

    cout << "Please provide the height of the rectangle: " << endl;
    cin >> height;
    cout << "Please provide the width of the rectangle: " << endl;
    cin >> width;

    cout << "The area of the shape is: " << width * height << std::endl;

    return 0;
}

int theme3_5 (){
    int v1 = 4, v2 = 5, v3 = 6;

    cout << "v1: " << v1 << ", v2: " << v2 << ", v3: " << v3 << endl;
    cout << "v1 / v2: " << v1 / v2 << endl;
    cout << "v1 % v2: " << v1 % v2 << endl;
    cout << "v1 * v2 / v3: " << v1 * v2 / v3 << endl;
    cout << "v1 * v2 % v3: " << v1 * v2 % v3 << endl;
    cout << "v1 <> v2: " << (v1 != v2) << endl;
    cout << "v1 OR v2: " << (v1 || v2) << endl;
    cout << "v1 AND v2: " << (v1 && v2) << endl;
    cout << "v1 + v2 - 1 <> v3: " << (v1 + v2 - 1 != v3) << endl;
}