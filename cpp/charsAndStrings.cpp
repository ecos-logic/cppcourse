#include <iostream>
#include <cstring>

using namespace std;

void workingWithChars(){
    char firstCharCollection[]="Developing";
    char secondCharCollection[]=" 1st";
    char thirdCharCollection[50];

    int length;

    strcpy (thirdCharCollection, firstCharCollection);
    length = strlen(firstCharCollection);
    cout << "Char length is: " << length << endl ;

    if(!strcmp(firstCharCollection, thirdCharCollection)) {
        if (strcmp(firstCharCollection, thirdCharCollection) == 0)
            cout << "Equals\n";
        else
            cout << "Different\n";
    }

    strcat(thirdCharCollection, secondCharCollection);

    cout << thirdCharCollection << endl;
    cout << strstr(firstCharCollection, "ve") << endl;

    cout << atoi("123") << endl;
    cout << atof("12.3") << endl;
}