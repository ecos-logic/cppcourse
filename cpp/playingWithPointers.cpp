#include <iostream>
#include <cmath>
#include "../headers/myflix.h"
#include "../headers/playingWithPointers.h"
static const int ResidentEvil1 = 0;

void pointersToBasicTypesAndStructs(){
    //-- -----------        SYNTAX             -------- --
    int *pointerToInt;
    int intValue = 5;

    pointerToInt = &intValue; //<<<<<<<<<<<<<<<<<<< PLEASE BEWARE1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    cout << pointerToInt << endl;
    cout << *pointerToInt << endl; //<<<<<<<<<<<<<<<<<<< PLEASE BEWARE2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //-- ---------------------------------------------- --

    movie movies[MOVIE_LENGTH];
    movie anotherMovies[MOVIE_LENGTH];
    movie *pointerToMovies;

    insertMovie(movies);
    insertMovie(anotherMovies);

    cout << "-- --------------------      \"MOVIES\"                    ------------ --" << endl;
    showMovie(movies[ResidentEvil1], ResidentEvil1);
    cout << "-- ------------------------------------------------------------------ --" << endl << endl << endl;

    anotherMovies[ResidentEvil1].director="REALLY NOT MATTERS";
    cout << "-- --------------------  \"ANOTHER MOVIES AFTER CHANGE DIRECTOR\" ------- --" << endl;
    showMovie(anotherMovies[ResidentEvil1], ResidentEvil1);
    cout << "-- ------------------------------------------------------------------ --" << endl << endl << endl;

    cout << "-- ------------------- \"DID THE MOVIES CHANGES AFTER CHANGE DIRECTOR?\"- --" << endl;
    showMovie(movies[ResidentEvil1], ResidentEvil1);
    cout << "-- ------------------------------------------------------------------ --" << endl << endl << endl;


    cout << "-- ----------- POINTER TO MOVIES ASSIGN TO MOVIES AND CHANGE DIRECTOR     ---- --" << endl;
    pointerToMovies = &movies[0]; //<<<<<<<<<<<<<<<<<<< PLEASE BEWARE1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    pointerToMovies->director="Paul W.S. Anderson"; //<<<<<<<<<<<<<<<<<<< PLEASE BEWARE2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    cout << "pointerToMovies->director=\"Paul W.S. Anderson\";" << endl;
    cout << "count << pointerToMovies->director: " << pointerToMovies->director << endl;
    cout << "-- ------------------------------------------------------------------ --" << endl << endl << endl;

    cout << "-- --------------------      \"MOVIES\"                    ------------ --" << endl;
    showMovie(movies[ResidentEvil1], ResidentEvil1);
    cout << "-- ------------------------------------------------------------------ --" << endl << endl << endl;

    cout << "-- --------------------      \"NEW MOVIE\"                    ------------ --" << endl;

    movie *pointerToMovie;

    if((pointerToMovie = new movie) == nullptr)
        cout << "Assign error\n";
    else {
        pointerToMovie->title = "Dr. Strange";
        cout << pointerToMovie->title << endl;
//        cout << (*pointerToMovie).title << endl;
        delete pointerToMovie;
    }
}

void pointersToFunctions(){
    float (*pointerToReturnFloatFunctionAndOneFloatParameter)(float);

    // -- --------------------------------------------------------------------- --//
    pointerToReturnFloatFunctionAndOneFloatParameter = circleAreaCalculator;
    cout << pointerToReturnFloatFunctionAndOneFloatParameter(10) << endl;

    // -- --------------------------------------------------------------------- --//
    pointerToReturnFloatFunctionAndOneFloatParameter = circlePerimeterCalculator;
    cout << pointerToReturnFloatFunctionAndOneFloatParameter(10) << endl;
}

float circleAreaCalculator(float radius){
    return M_1_PI * radius * radius;
}

float circlePerimeterCalculator(float radius){
    return 2.0F * M_1_PI * radius;
}

void pointersAndArrays(){
    int sortedArray[5] = {1, 2, 3, 4,5};

    cout << "-- ----------------------------------------------------- --" << endl;
    cout << "int *pointerToSortedArray = &sortedArray [0];" << endl;
    int *pointerToSortedArray = sortedArray;
    cout << "pointerToSortedArray[2]: " << pointerToSortedArray[2] << endl;
    cout << "*(sortedArray + 2): " << *(sortedArray + 2) << endl;
    cout << "-- ----------------------------------------------------- --" << endl << endl;



    cout << "-- ----------------------------------------------------- --" << endl;
    cout << "pointerToSortedArray = &sortedArray [1];" << endl;
    pointerToSortedArray = sortedArray + 1 ;
    cout << "pointerToSortedArray[2]: " << pointerToSortedArray[2] << endl;
    cout << "*(sortedArray + 2): " << *(sortedArray + 2) << endl;
    cout << "-- ----------------------------------------------------- --" << endl;
}

void byValueFunction(movie movie){
    movie.director = "it doesn't matter I'm a poor 'by value function'";
}

void byReferenceFunction(movie *movie){
    movie->director = "The greatest director in the world!!!!! (... maybe not)";
}

void pointersAndArraysAndStructs() {
    movie *pointerToMovie;
    movie movies[MOVIE_LENGTH];
    pointerToMovie = movies;
//    pointerToMovie = new movie[MOVIE_LENGTH];

    insertMovie(pointerToMovie);
    insertMovie(movies);

    byValueFunction(movies[0]);
    byValueFunction(*pointerToMovie);

    cout << movies[0].director << endl;
    cout << pointerToMovie->director << endl;
    cout  << endl << endl;

    byReferenceFunction(&movies[0]);
    byReferenceFunction(pointerToMovie);

    cout << movies[0].director << endl;
    cout << pointerToMovie->director << endl;

    movies[0].director = "HELOOOOO!!!! " + pointerToMovie->director;
    cout << movies[0].director << endl;
    cout << pointerToMovie->director << endl;
}