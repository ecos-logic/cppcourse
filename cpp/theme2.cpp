#include <iostream>
using namespace std;

int theme2_1() {
    int number1;
    int number2;
    int sum;

    cout << "Provide me the first number: " << endl;
    cin >> number1;

    cout << "Provide me the second number: " << endl;
    cin >> number2;

    sum = number1 + number2;

    cout << "The result to add the two numbers are: " << sum;

    return 0;
}

int theme2_2(){
    int lowerNumber;
    int number1;
    int number2;

    cout << "Provide me the first number: " << endl;
    cin >> number1;

    cout << "Provide me the second number: " << endl;
    cin >> number2;

    if(number1<=number2){
        lowerNumber = number1;
    }else{
        lowerNumber = number2;
    }

    cout << "The lower number is: " << lowerNumber << endl;
    return 0;
}