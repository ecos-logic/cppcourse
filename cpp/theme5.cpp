int absoluteValue(int x)
{
    if ( x < 0 )
        return (-1 * x);
    else
        return (x);
}

void interchange(int a, int b) {
    int temp;
    temp = a;
    a = b;
    b = temp;
}

int factorial(int n) {
    if ( n == 0)
        return 1; // Base case
    else
        return n * factorial(n-1);
}

int pow (int b, int n) {
    int power;
    if (n <= 0) // Base case
        return 1;
    if (n % 2 == 0) { // n odd
        power = pow(b, n / 2);
        return power * power;
    }
    else { // n dot
        power = pow(b, (n - 1) / 2);
        return power * power * b;
    }
}