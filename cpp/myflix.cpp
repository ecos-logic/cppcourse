#include <iostream>
#include <fstream>
#include "../headers/myflix.h"

using namespace std;

void insertMovie(movie movies[MOVIE_LENGTH]) {

    for(int i=0;i<MOVIE_LENGTH;i++){
        movies[i].title="";
        movies[i].director="";
        movies[i].length=0;
        for (int j = 0; j <CAST_LENGTH; j++) {
            movies[i].cast[j]="";
        }
    }

    int index = 0;
    movies[index].title = "Resident Evil 1";
    movies[index].director = "No matters";
    movies[index].length = 100;
    movies[index].cast[0] = "Mira Jorodoswki";
    movies[index].cast[1] = "Mira Jorodoswki";
    movies[index].cast[2] = "Mira Jorodoswki";
    movies[index].cast[3] = "Mira Jorodoswki";
    movies[index].cast[4] = "Mira Jorodoswki";

    index = 1;
    movies[index].title = "Resident Evil 2";
    movies[index].director = "No matters";
    movies[index].length = 101;
    movies[index].cast[0] = "Mira Jorodoswki";
    movies[index].cast[1] = "Mira Jorodoswki";
    movies[index].cast[2] = "Mira Jorodoswki";
    movies[index].cast[3] = "Mira Jorodoswki";
    movies[index].cast[4] = "Mira Jorodoswki";

}

void insertSeries(series seriesCollection[SERIES_LENGTH]) {
    for (int i = 0; i < SERIES_LENGTH; i++) {
        seriesCollection[i].title = "Breaking Bad";

        for (int j = 0; j < CHAPTER_LENGTH; j++) {
            seriesCollection[i].chapters[j].title = "Pilot";
            seriesCollection[i].chapters[j].length = 45;
        }
    }

}

void showMovies(movie movies[MOVIE_LENGTH]) {
    for (int i = 0; i < MOVIE_LENGTH; i++) {
        cout << "The title of the movie number " << i << " is " << movies[i].title << endl;
        cout << "\tThe director of the movie number " << i << " is " << movies[i].director << endl;
        cout << "\tThe length of the movie number " << i << " is " << movies[i].length << endl;
        cout << endl;

        for (int j = 0; j < CAST_LENGTH; j++) {
            cout << "\t\tThe actor number " << j << " from " << movies[i].title << " is " << movies[i].cast[j] << endl;
        }

        cout << endl;
        cout << endl;
    }
}

void showMovie(movie theMovie, int i) {
    cout << "The title of the movie number " << i << " is " << theMovie.title << endl;
    cout << "\tThe director of the movie number " << i << " is " << theMovie.director << endl;
    cout << "\tThe length of the movie number " << i << " is " << theMovie.length << endl;
    cout << endl;

    for (int j = 0; j < CAST_LENGTH; j++) {
        cout << "\t\tThe actor number " << j << " from " << theMovie.title << " is " << theMovie.cast[j] << endl;
    }

    cout << endl;
    cout << endl;
}


void showSeries(series seriesCollection[SERIES_LENGTH]) {
    for (int i = 0; i < SERIES_LENGTH; i++) {
        cout << "The title of the series number " << i << " is " << seriesCollection[i].title << endl;

        for (int j = 0; j < CHAPTER_LENGTH; j++) {
            cout << "\tThe title of the chapter number " << j << " from " << seriesCollection[i].title << " is "
                 << seriesCollection[i].chapters[j].title << endl;
            cout << "\tThe length of the chapter number " << j << " from " << seriesCollection[i].title << " is "
                 << seriesCollection[i].chapters[j].length << endl;
            cout << endl;
        }

        cout << endl;
        cout << endl;
    }
}

void showASeries(series series, int i) {
    cout << "The title of the series number " << i << " is " << series.title << endl;

    for (int j = 0; j < CHAPTER_LENGTH; j++) {
        cout << "\tThe title of the chapter number " << j << " from " << series.title << " is " << series.chapters[j].title << endl;
        cout << "\tThe length of the chapter number " << j << " from " << series.title << " is " << series.chapters[j].length << endl;
        cout << endl;
    }

    cout << endl;
    cout << endl;
}

void showIntercalate(movie movies[MOVIE_LENGTH], series seriesCollection[SERIES_LENGTH]) {
    for (int i = 0; i < MOVIE_LENGTH; i++) {
        if(!movies[i].title.empty())
            showMovie(movies[i], i);
        if (i % 2 != 0)
            showASeries(seriesCollection[i / 2], i / 2);
    }
}

string movieAsString(const movie &movie) {
    string movieAsString;
    movieAsString += movie.title + "\n";

    movieAsString += "\t" + movie.director + "\n";
    movieAsString += "\t" + to_string(movie.length) + "\n";

    for (const auto & cast : movie.cast) {
        movieAsString += "\t\t" + cast  + "\n";
    }

    return movieAsString;
}

string myReplace(string from,string what,string newString) {
    return from.replace(from.find(what),what.length(),newString);
}

void readFromFile(movie *movies) {
    ifstream fileOfMoviesToRead;

    fileOfMoviesToRead.open("../files/moviesAsTextToRead.txt",ios::in);

    for (int i = 0; i < 2; i++) {
        getline(fileOfMoviesToRead, movies[i].title);

        string aux;
        getline(fileOfMoviesToRead, aux);
        movies[i].length = stoi(aux);

        getline(fileOfMoviesToRead, movies[i].director);
        movies[i].director=myReplace(movies[i].director,"\t","");

        for (auto & actor : movies[i].cast) {
            getline(fileOfMoviesToRead, actor);
            actor=myReplace(actor, "\t\t", "");
        }
        showMovie(movies[i], i);
    }

    fileOfMoviesToRead.close();
}