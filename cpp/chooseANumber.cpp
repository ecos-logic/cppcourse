#include <iostream>

using namespace std;
static const int numberOfAttempts = 10;

void randomNumber() {
    int randomlyChosenNumber;
    srand(time(NULL));
    randomlyChosenNumber = rand() % 100;
    int userSelectedNumber;
    bool winner = false;
    int i = 0;
    while (i < numberOfAttempts && !winner) {
        cout << "Insert the number" << endl;
        cin >> userSelectedNumber;
        cout << endl;



        if (userSelectedNumber == randomlyChosenNumber) {
            winner = true;
        } else if (userSelectedNumber > randomlyChosenNumber) {
            cout << "The number is bigger than the number I selected" << endl;
        } else if (userSelectedNumber < randomlyChosenNumber) {
            cout << "The number is lower than the number I selected" << endl;
        }
        i++;
    }

    if (winner)
        cout << "You won!" << endl;
    else
        cout << "You lose :-(!" << endl;
}